package com.smartapps.testdesing;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Toast;

import java.security.PrivateKey;

public class SecondActivity extends AppCompatActivity {
    private FloatingActionButton fab_plus;
    private FloatingActionButton fab_one;
    private FloatingActionButton fab_two;
    private FloatingActionButton fab_wave;
    private FloatingActionButton fab_wave2;
    private Animation aFabOpen;
    private Animation aFabWave;
    private Animation aFabWave2;
    private Animation aFabClose;
    private Animation aFabRotateClockwise;
    private Animation aFabRotateAnticlockwise;
    private boolean isOpen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        init();



    }


    public void init(){
        fab_wave = (FloatingActionButton)findViewById(R.id.fab_wave);
        fab_plus = (FloatingActionButton) findViewById(R.id.fab_plus);
        fab_one = (FloatingActionButton) findViewById(R.id.fab_one);
        fab_two = (FloatingActionButton) findViewById(R.id.fab_two);
        aFabOpen = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_open);
        aFabWave = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.button_wave);

        aFabClose = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fab_close);
        aFabRotateClockwise = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_clockwise);
        aFabRotateAnticlockwise = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.rotate_anticlockwise);

        aFabWave.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                fab_wave.clearAnimation();
                fab_wave.startAnimation(aFabWave);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });

        fab_wave.startAnimation(aFabWave);
        //fab_wave2.startAnimation(aFabWave2);
        fab_plus.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View view) {
                if (isOpen){
                    fab_one.startAnimation(aFabClose);
                    fab_two.startAnimation(aFabClose);
                    fab_plus.startAnimation(aFabRotateAnticlockwise);
                    fab_one.setClickable(false);
                    fab_two.setClickable(false);
                    isOpen = false;

                }else {
                    fab_one.startAnimation(aFabOpen);
                    fab_two.startAnimation(aFabOpen);
                    fab_plus.startAnimation(aFabRotateClockwise);
                    fab_one.setClickable(true);
                    fab_two.setClickable(true);
                    isOpen = true;

                }

            }
        });
        fab_one.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getBaseContext(),"FAB one pressed!",Toast.LENGTH_SHORT).show();
            }
        });
        fab_two.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getBaseContext(),"FAB two pressed!",Toast.LENGTH_SHORT).show();
            }
        });



    }



}
